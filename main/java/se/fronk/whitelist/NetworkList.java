package se.fronk.whitelist;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fronkan on 2017-03-25.
 */

public class NetworkList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.networklist);


        //TODO: FIX HOW TO COLOR ITEMS. make own simple_list_item?
        List<WifiConfiguration> networkList = getStoredNetworks();

        List<String> ssidList = new ArrayList<String>();
        if(networkList != null) {
            for (int i = 0; i < networkList.size(); i++) {
                ssidList.add(networkList.get(i).SSID);
            }
        }
        else{
            ssidList.add("NO NETWORKS AT ALL");
        }
        setUpListView(ssidList);
    }

    private List<WifiConfiguration> getStoredNetworks(){
        Context appContext = getApplicationContext();
        WifiManager wifiManager = (android.net.wifi.WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);
        return wifiManager.getConfiguredNetworks();
    }
    private void setUpListView(List list){
        ListView listView = (ListView) findViewById(R.id.networklist) ;
        ArrayAdapter adapter = new ArrayAdapter<WifiConfiguration>(getApplicationContext(), android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}
