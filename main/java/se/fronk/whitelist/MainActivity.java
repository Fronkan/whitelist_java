package se.fronk.whitelist;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import se.fronk.whitelist.database.DBHandler;

public class MainActivity extends AppCompatActivity {

    DBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHandler = new DBHandler(this, null);


        //TODO: FIX HOW TO COLOR ITEMS. make own simple_list_item?
        List<String> networkList = getWhitelistNetworks();

       List<String> ssidList = new ArrayList<String>();
        if(networkList != null) {
            setUpListView(ssidList);
        }
        else{
            ssidList.add("NO NETWORKS IN WHITELIST");
        }
        setUpListView(ssidList);
    }


    private List<String> getWhitelistNetworks(){
        return dbHandler.dbSSIDArray();
    }
    private void setUpListView(List list){
        ListView listView = (ListView) findViewById(R.id.whiteList) ;
        ArrayAdapter adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onClickListAdd(View view) {


        Intent addNetworkFromList = new Intent(this, NetworkList.class);

        startActivity(addNetworkFromList);


        //TODO: Fully implement the add network to DB in this and for single add
        //Testing the database. Connection. IT SEEMS TO WORK, YAAAY.
        /*dbHandler.addNetworkToWhitelist("TEST");
        setUpListView(dbHandler.dbSSIDArray());*/
    }


}
