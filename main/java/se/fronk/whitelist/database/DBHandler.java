package se.fronk.whitelist.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fronkan on 2017-03-18.
 */

public class DBHandler extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME ="whitelist.db";
    public static final String TABLE_WHITELIST ="whiteListe";
    public static final String COLUMN_ID ="_id";
    public static final String COLUMN_SSID ="SSID";

    //TODO: Create this when done with the fetching of networkdata
    public DBHandler(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DB_NAME, factory, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQLCreateTable = "CREATE TABLE " + TABLE_WHITELIST + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                COLUMN_SSID + " TEXT " +
                " );";

        db.execSQL(SQLCreateTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_WHITELIST);
        onCreate(db);
    }

    public void addNetworkToWhitelist(String networkSSID){
        ContentValues values = new ContentValues();
        values.put(COLUMN_SSID,networkSSID);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_WHITELIST, null, values);
        db.close();
    }

    public void deleteNetworkFromWhitelist(String networkSSID){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_WHITELIST + " WHERE " + COLUMN_SSID + "=\"" + networkSSID + "\";");
    }

    public String dbToString(){
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_WHITELIST + " WHERE 1";


        //CYRESOR PINTS TO A LOCATION IN YOUR RESULTS

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        while (!c.isAfterLast()){
            if(c.getString(c.getColumnIndex(COLUMN_SSID)) != null){
                dbString += c.getString(c.getColumnIndex(COLUMN_SSID));
                dbString += "\n";
            }
            c.moveToNext();
        }
        db.close();
        return dbString;
    }

    public List<String> dbSSIDArray(){
        List<String> SSIDList = new ArrayList<String>();
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_WHITELIST + " WHERE 1";

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        while (!c.isAfterLast()){
            if(c.getString(c.getColumnIndex(COLUMN_SSID)) != null){
                SSIDList.add(c.getString(c.getColumnIndex(COLUMN_SSID)));
            }
            c.moveToNext();
        }
        db.close();
        return SSIDList;
    }


}
